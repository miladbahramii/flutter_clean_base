import 'package:flutter_clean_base/features/login/domain/entities/jwt_token.dart';
import 'package:flutter_clean_base/features/login/domain/repositories/login_repository.dart';
import 'package:flutter_clean_base/features/login/domain/usecases/login.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockLoginRepository extends Mock implements LoginRepository {}

main() {
  MockLoginRepository mockLoginRepository;
  Login usecase;

  setUp(() async {
    mockLoginRepository = MockLoginRepository();
    usecase = Login(mockLoginRepository);
  });

  final JwtToken tToken = JwtToken(token: 'testToken');

  test('should login and get JWT TOKEN', () async {
    when(mockLoginRepository.login('milad', 'test'))
        .thenAnswer((_) async => Right(tToken.token));

    final result =
        await usecase(LoginParams(username: 'milad', password: 'test'));

    expect(result, Right(tToken));
  });
}
