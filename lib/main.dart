import 'package:flutter/material.dart';
import 'package:flutter_clean_base/features/home/presentation/pages/home_page.dart';
import 'package:flutter_clean_base/features/login/presentation/pages/login_page.dart';
import 'package:flutter_clean_base/injection_container.dart' as di;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'app_localizations.dart';

void main() {
  di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),

      // App localization
      supportedLocales: [
        Locale('en', 'US'),
        Locale('fa', 'IR'),
      ],
      localizationsDelegates: [
        // A class which loads the translations from JSON files
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      // Returns a locale which will be used by the app
      localeResolutionCallback: (locale, supportedLocales) {
        //! in loop zabane app ro be zabane device taghir mide.
        // for (var supportedLocale in supportedLocales) {
        //   if (supportedLocale.languageCode == locale.languageCode &&
        //       supportedLocale.countryCode == locale.countryCode) {
        //     return supportedLocale;
        //   }
        // }
        //? first locale from supportedLocal array.
        return supportedLocales.first;
      },
      initialRoute: '/',
      routes: {
        '/': (context) => LoginDetectorWidget(),
        '/home': (context) => HomePage(),
        '/login': (context) => LoginPage(),
      },
    );
  }
}

class LoginDetectorWidget extends StatelessWidget {
  const LoginDetectorWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // di.sl<FlutterSecureStorage>().delete(key: 'jwt_token');
    return FutureBuilder<String>(
      future: di.sl<FlutterSecureStorage>().read(key: 'jwt_token'),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        return snapshot.data != null ? HomePage() : LoginPage();
      },
    );
  }
}
