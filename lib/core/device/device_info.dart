import 'package:device_info/device_info.dart';
import 'dart:io' show Platform;

import 'package:equatable/equatable.dart';

abstract class DeviceInfo {
  Future<Device> get info;
}

class DeviceInfoImpl implements DeviceInfo {
  final DeviceInfoPlugin deviceInfoPlugin;

  DeviceInfoImpl(this.deviceInfoPlugin);

  @override
  Future<Device> get info async{
    if(Platform.isAndroid) {
      final AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      return Device(os: 'android', model: androidInfo.model);
    }
    else if(Platform.isIOS) {
      final IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
      return Device(
        os: 'ios',
        model: iosInfo.utsname.machine
      );
    }
    else if(Platform.isWindows) {
      return Device(os: 'windows');
    }
    else if(Platform.isMacOS) {
      return Device(os: 'mac_os');
    }
    else if(Platform.isLinux) {
      return Device(os: 'linux');
    }
    else if(Platform.isFuchsia) {
      return Device(os: 'fuchsia');
    }
    else {
      return(Device(os: 'undefined'));
    }
  }
}

class Device extends Equatable {
  final String os;
  final String model;

  Device({this.os, this.model});

  @override
  List<Object> get props => [os, model];
}
