class ServerException implements Exception {
  final String statusCode;

  ServerException(this.statusCode);
}

class CacheException implements Exception {}

class NetworkException implements Exception {}
