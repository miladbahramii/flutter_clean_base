class DateConvertor {
  DateTime increaseNowWithMinutes(int minutes) {
    final now = DateTime.now().millisecondsSinceEpoch;
    final timestamp = now + (minutes * 60000);
    return new DateTime.fromMillisecondsSinceEpoch(timestamp);
  }
}