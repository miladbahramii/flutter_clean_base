import 'package:dartz/dartz.dart';
import 'package:flutter_clean_base/core/error/failures.dart';
import 'package:intl/intl.dart';

class InputConvertor {
  static final tomanFormater = new NumberFormat("#,##0", "fa_IR");

  Either<Failure, int> stringToUnsignedInteger(String str) {
    try {
      final integer = int.parse(str);
      if (integer < 0) throw FormatException();
      return Right(integer);
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }

  String numberToCurrency(int n) {
    return tomanFormater.format(n);
  }

  String stringToCurrency(String str) {
    final integer = int.parse(str);
    if (integer >= 0) {
      return tomanFormater.format(integer);
    } else {
      return '0';
    }
  }
}
