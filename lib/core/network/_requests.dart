// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart';
// import 'dart:convert';
// import 'package:path/path.dart';
// import 'dart:io';
// import 'package:shabesh_application/services/helper.dart';

// class ResponseQuery {
//   int statusCode;
//   dynamic result;
//   String message;
//   bool hasError;

//   ResponseQuery({this.statusCode, this.result, this.message, this.hasError});
// }

// class Requests {
//   String apiUrl = Helper.apiUrl;
//   String partUrl = '/api';
//   String dest; // location name for the UI
//   String token; // location name for the UI

//   Map<String, String> heads = {
//     'Content-Type': 'application/json; charset=utf-8',
//     'X-Requested-With': 'XMLHttpRequest',
//     'Authorization': 'Bearer ',
//   };

//   Requests({@required this.dest});

//   Future<ResponseQuery> customFetch(
//       {String customUrl, Map<String, String> data, token}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }

//     var uri = Uri.https(apiUrl, '$partUrl/$customUrl', data);

//     Response response = await get(
//       uri,
//       headers: heads,
//     );

//     return this.prepareResponse(response);
//   }

//   Future<ResponseQuery> customPost(
//       {String customUrl, Map<String, String> data, token}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }

//     var uri = Uri.https(apiUrl, '$partUrl/$customUrl');
//     Response response =
//         await post(uri, headers: heads, body: json.encode(data));

//     return this.prepareResponse(response);
//   }

//   Future<ResponseQuery> fetchItems(
//       {Map<String, String> data, String token}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }

//     var uri = Uri.https(apiUrl, '$partUrl/$dest', data);
//     Response response = await get(
//       uri,
//       headers: heads,
//     );

//     return this.prepareResponse(response);
//   }

//   Future<ResponseQuery> save({Map<String, String> data, String token}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }
//     Response response;
//     if (data.containsKey('id')) {
//       var uri = Uri.https(apiUrl, '$partUrl/$dest/${data["id"]}');
//       response = await put(uri, headers: heads, body: json.encode(data));
//     } else {
//       var uri = Uri.https(apiUrl, '$partUrl/$dest');
//       response = await post(uri, headers: heads, body: json.encode(data));
//     }

//     return this.prepareResponse(response);
//   }

//   Future<ResponseQuery> fetchItem(
//       {String id, String token, Map<String, String> data}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }

//     var uri = Uri.https(apiUrl, '$partUrl/$dest/$id', data);
//     Response response = await get(
//       uri,
//       headers: heads,
//     );

//     return this.prepareResponse(response);
//   }

//   Future<ResponseQuery> remove({id, String token}) async {
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }

//     var uri = Uri.https(apiUrl, '$partUrl/$dest/$id');
//     Response response = await delete(
//       uri,
//       headers: heads,
//     );

//     return this.prepareResponse(response);
//   }

//   upload(
//       {File imageFile,
//       String itemType,
//       String subFolder,
//       String itemId,
//       String token,
//       Function onComplete,
//       String usedAs = ''}) async {
//     // open a bytestream
//     if (!Helper.isNullEmptyOrFalse(token)) {
//       heads['Authorization'] = 'Bearer $token';
//     }
//     var stream = new ByteStream(imageFile.openRead());
//     stream.cast();
//     // get file length
//     var length = await imageFile.length();

//     // string to uri
//     var uri = Uri.https(apiUrl, '$partUrl/images');
//     // create multipart request
//     var request = new MultipartRequest("POST", uri);

//     request.headers.addAll(heads);

//     // multipart that takes file
//     var multipartFile = new MultipartFile('file', stream, length,
//         filename: basename(imageFile.path));

//     request.fields.addAll({
//       'item_id': itemId,
//       'item_type': itemType,
//       'sub_folder': subFolder,
//       'used_as': usedAs
//     });
//     // add file to multipart
//     request.files.add(multipartFile);

//     // send
//     var response = await request.send();

//     if (response.statusCode == 200) {
//       response.stream.transform(utf8.decoder).listen((value) {
//         onComplete(jsonDecode(value));
//       });
//     }
//   }

//   static bool isNullEmptyOrFalse(Object o) =>
//       o == null || false == o || "" == o;

//   static bool isNullEmptyFalseOrZero(Object o) =>
//       o == null || false == o || 0 == o || "" == o;

//   ResponseQuery prepareResponse(Response response) {
//     var message = '';

//     try {
//       Map data = jsonDecode(response.body);
//       switch (response.statusCode) {
//         case 200:
//           if (data['success']) {
//             return ResponseQuery(
//                 statusCode: 200,
//                 message: data.containsKey('message') ? data['message'] : '',
//                 result: data['object'],
//                 hasError: false);
//           } else {
//             return ResponseQuery(
//                 statusCode: 200,
//                 message: data['message'],
//                 result: data['message'],
//                 hasError: true);
//           }

//           break;
//         case 422:
//           if (data.containsKey('errors')) {
//             data['errors'].keys.forEach((key) {
//               message += data['errors'][key][0] + '\r\n';
//             });
//           }
//           break;

//         case 401:
//           message = 'لطفا مجددا وارد حساب کاربری خود شوید .';
//           break;
//         case 0:
//         case 503:
//           message = 'اتصال به اینترنت را بررسی نمایید';
//           break;
//         case 404:
//           message = 'یافت نشد .';
//           break;
//         default:
//           message = 'خطای دریافت داده ها';
//           break;
//       }
//     } catch (e) {
//       print('cautch error: $e');
//       message = 'خطای دریافت داده ها';
//     }

//     return ResponseQuery(
//         statusCode: 200, message: message, result: message, hasError: true);
//   }
// }
