import 'dart:convert';
import 'package:flutter_clean_base/core/error/exeptions.dart';
import 'package:flutter_clean_base/core/network/network_info.dart';
import 'package:http/http.dart' as http;

abstract class Request {
  Future<http.Response> post(
      String url, Map<String, String> header, Map<String, String> params);
  Future<http.Response> get();
}

class RequestImpl implements Request {
  final http.Client client;
  final NetworkInfo networkInfo;

  RequestImpl({this.client, this.networkInfo});

  @override
  Future<http.Response> get() {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<http.Response> post(
    String url,
    Map<String, String> header,
    Map<String, String> params,
  ) async {
    if (!await networkInfo.isConnected) {
      throw NetworkException();
    }

    final response = await client.post(
      url,
      headers:
          header.length > 0 ? header : {'Content-Type': 'application/json'},
      body: jsonEncode(params),
    );
    print('request url: $url');
    print('status code is ${response.statusCode}');
    final statusCode = _statusChecker(response);
    if (response.statusCode == 200)
      return response;
    else
      throw ServerException(statusCode);
  }

  String _statusChecker(http.Response response) {
    switch (response.statusCode) {
      case 200:
        return 'success';
        break;
      case 404:
        return 'not_found';
        break;
      case 400:
        return 'bad_request';
        break;
      case 651:
        return 'not_found_exception';
        break;
      case 653:
        return 'orleans_exception';
        break;
      case 603:
        return 'selected_validation_exception';
        break;
      default:
        return 'undefined_status_code';
    }
  }
}
