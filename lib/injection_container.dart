import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter_clean_base/core/device/device_info.dart';
import 'package:flutter_clean_base/core/network/network_info.dart';
import 'package:flutter_clean_base/core/utils/input_convertor.dart';
import 'package:flutter_clean_base/features/login/data/datasources/login_local_data_source.dart';
import 'package:flutter_clean_base/features/login/data/datasources/login_remote_data_source.dart';
import 'package:flutter_clean_base/features/login/data/repositories/login_repository_impl.dart';
import 'package:flutter_clean_base/features/login/domain/repositories/login_repository.dart';
import 'package:flutter_clean_base/features/login/domain/usecases/logout.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'core/network/request.dart';
import 'core/utils/date_convertor.dart';
import 'features/login/domain/usecases/login.dart';
import 'features/login/presentation/cubit/login_cubit.dart';

final sl = GetIt.instance;

void init() {
  //! Features
  injectLoginFeature();
  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<DeviceInfo>(() => DeviceInfoImpl(sl()));
  sl.registerLazySingleton<Request>(
      () => RequestImpl(client: sl(), networkInfo: sl()));
  sl.registerLazySingleton(() => DateConvertor());
  sl.registerLazySingleton(() => InputConvertor());
  //! Externals
  sl.registerLazySingleton(() => FlutterSecureStorage());
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => DeviceInfoPlugin());
}

void injectLoginFeature() {
  // Cubit
  sl.registerFactory(() => LoginCubit(
        login: sl(),
        logout: sl(),
      ));
  // Usecases
  sl.registerLazySingleton(() => Login(sl()));
  sl.registerLazySingleton(() => Logout(sl()));
  // Repository
  sl.registerLazySingleton<LoginRepository>(() => LoginRepositoryImpl(
      localDataSource: sl(), networkInfo: sl(), remoteDataSource: sl()));
  // Data Sources
  sl.registerLazySingleton<LoginLocalDataSource>(() => LoginLocalDataSourceImpl(
      secureStorage: sl(), dateConvertor: sl(), inputConvertor: sl()));
  sl.registerLazySingleton<LoginRemoteDataSource>(() =>
      LoginRemoteDataSourceImpl(
          client: sl(), deviceInfo: sl(), request: sl(), secureStorage: sl()));
}
