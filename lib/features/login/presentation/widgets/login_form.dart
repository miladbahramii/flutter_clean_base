import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_base/app_localizations.dart';
import 'package:flutter_clean_base/features/login/presentation/cubit/login_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


enum Fields { username, password }

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String username;
  String password;

  _LoginFormState();

  @override
  void initState() {
    super.initState();
    listenToCatchError();
  }

  void submitForm() async {
    await context.bloc<LoginCubit>().loginUser(username, password);
  }

  void listenToCatchError() {
    context.bloc<LoginCubit>().listen((state) {
      if (state is Error) {
        showFlushbar(AppLocalizations.of(context).translate(state.message), false);
      }
      else if (state is LoginStatus) {
        Navigator.pushReplacementNamed(context, '/home');
      }
    });
  }

  showFlushbar(String message, bool isComplete) async {
    Flushbar(
      // title: "Error",
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.red,
      message: message,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  void onChangeFields(Fields field, String fieldText) {
    switch (field) {
      case Fields.username:
        username = fieldText;
        break;
      case Fields.password:
        password = fieldText;
        break;
      default:
        throw UnimplementedError();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: [
            TextFormField(
              initialValue: username,
              decoration: InputDecoration(
                hintText: '',
                labelText: AppLocalizations.of(context).translate('username'),
                alignLabelWithHint: true,
              ),
              // validator: (value) => _formValidator(value),
              onChanged: (text) => onChangeFields(Fields.username, text),
              // validator: (value) => _validateForm(value),
            ),
            Expanded(
              child: TextFormField(
                initialValue: password,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: '',
                  labelText: AppLocalizations.of(context).translate('password'),
                  alignLabelWithHint: true,
                ),
                // validator: (value) => _formValidator(value),
                onChanged: (text) => onChangeFields(Fields.password, text),
                // validator: (value) => _validateForm(value),
              ),
            ) ,
            Container(
              width: double.infinity,
              child: RaisedButton(
                color: Colors.blue,
                onPressed: () => submitForm(),
                child: BlocBuilder<LoginCubit, LoginState>(
                  builder: (BuildContext context, state) {
                    if (state is Loading) {
                      return SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          ));
                    }
                    return Text(
                      AppLocalizations.of(context).translate('submit'),
                      style: TextStyle(color: Colors.white),
                    );
                  },
                ),
              ),
            )
          ],
        ));
  }
}
