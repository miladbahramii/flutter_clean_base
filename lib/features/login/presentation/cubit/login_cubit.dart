import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_clean_base/core/usecases/usecase.dart';
import 'package:flutter_clean_base/features/login/domain/usecases/logout.dart';
import '../../../../core/error/failures.dart';
import '../../domain/usecases/login.dart';
part 'login_state.dart';

const SERVER_FAILURE_MESSAGE = 'Server Failure.';
const CACHE_FAILURE_MESSAGE = 'Cache Failure.';
const NETWORK_FAILURE_MESSAGE =
    'Network failure. Please check your internet connection.';

class LoginCubit extends Cubit<LoginState> {
  final Login login;
  final Logout logout;
  LoginCubit({this.login, this.logout}) : super(Empty());

  Future<void> loginUser(String username, String password) async {
    emit(Loading());
    final token =
        await login(LoginParams(username: username, password: password));
    _tokenLoadedOrErrorState(token);
  }

  Future<void> logoutUser() async{
    await logout(NoParams());
  }

  void _tokenLoadedOrErrorState(Either<Failure, String> token) {
    token.fold((failure) {
      emit(Error(message: _mapFailureToMessage(failure)));
    }, (tokenStatus) {
      emit(LoginStatus(message: tokenStatus));
    });
  }

  String _mapFailureToMessage(Failure failure) {
    if (failure is ServerFailure)
      return failure.statusText;
    else if(failure is CacheFailure)
      return CACHE_FAILURE_MESSAGE;
    else if(failure is NetworkFailure)
      return 'network_failure';
    else
      return 'Unexpected error';
  }
}
