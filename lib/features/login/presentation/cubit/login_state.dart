part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class Empty extends LoginState {}

class Loading extends LoginState {}

class Error extends LoginState {
  final String message;

  Error({this.message});

  @override
  List<Object> get props => [message];
}

class LoginStatus extends LoginState {
  final String message;

  LoginStatus({this.message});

  @override
  List<Object> get props => [message];
}