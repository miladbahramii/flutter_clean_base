import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_base/app_localizations.dart';
import 'package:flutter_clean_base/features/login/presentation/cubit/login_cubit.dart';
import 'package:flutter_clean_base/features/login/presentation/widgets/login_form.dart';

import '../../../../injection_container.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => sl<LoginCubit>(),
          child: LoginForm(),
        ),
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).translate('login'))),
      ),
    );
  }
}
