import 'package:equatable/equatable.dart';

class JwtToken extends Equatable {
  final String token;
  final String refreshToken;
  final int tokenExpire;
  final String expireDate;

  JwtToken({this.token, this.refreshToken, this.tokenExpire, this.expireDate});

  @override
  // TODO: implement props
  List<Object> get props => [token, refreshToken, tokenExpire];
}
