import 'package:flutter_clean_base/core/error/failures.dart';
import 'package:flutter_clean_base/core/usecases/usecase.dart';
import 'package:flutter_clean_base/features/login/domain/repositories/login_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class RefreshToken extends UseCase<String, NoParams> {
  final LoginRepository repository;

  RefreshToken(this.repository);

  @override
  Future<Either<Failure, String>> call(NoParams params) async {
    return await repository.logout();
  }
}
