import 'package:flutter_clean_base/core/error/failures.dart';
import 'package:flutter_clean_base/core/usecases/usecase.dart';
import 'package:flutter_clean_base/features/login/domain/repositories/login_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class Login extends UseCase<String, LoginParams> {
  final LoginRepository repository;

  Login(this.repository);

  @override
  Future<Either<Failure, String>> call(params) async {
    return await repository.login(params.username, params.password);
  }
}

class LoginParams {
  final String username;
  final String password;

  LoginParams({@required this.username, @required this.password});
}
