import 'package:flutter_clean_base/core/error/failures.dart';
import 'package:flutter_clean_base/features/login/domain/entities/jwt_token.dart';
import 'package:dartz/dartz.dart';

abstract class LoginRepository {
  Future<Either<Failure, String>> login(String username, String password);
  Future<Either<Failure, String>> logout();
  Future<Either<Failure, String>> refreshToken();
}
