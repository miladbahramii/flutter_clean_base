import 'package:dartz/dartz.dart';
import 'package:flutter_clean_base/core/error/exeptions.dart';
import 'package:flutter_clean_base/core/error/failures.dart';
import 'package:flutter_clean_base/core/network/network_info.dart';
import 'package:flutter_clean_base/features/login/data/datasources/login_local_data_source.dart';
import 'package:flutter_clean_base/features/login/data/datasources/login_remote_data_source.dart';
import 'package:flutter_clean_base/features/login/domain/entities/jwt_token.dart';
import 'package:flutter_clean_base/features/login/domain/repositories/login_repository.dart';
import 'package:meta/meta.dart';

class LoginRepositoryImpl implements LoginRepository {
  final LoginRemoteDataSource remoteDataSource;
  final LoginLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  LoginRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, String>> login(
      String username, String password) async {
    try {
      final token = await remoteDataSource.getToken(username, password);
      localDataSource.cacheToken(token);
      return Right('token is cached.');
    } on ServerException catch (e) {
      return Left(ServerFailure(e.statusCode));
    } on NetworkException {
      return Left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> logout() async {
    try {
      JwtToken token = await localDataSource.getToken();
      await remoteDataSource.invalidateToken(token.token);
      await localDataSource.deleteToken();
      return Right('token is removed.');
    } on CacheException {
      throw Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, String>> refreshToken() {
    // TODO: implement refreshToken
    throw UnimplementedError();
  }
}
