import 'package:flutter_clean_base/features/login/domain/entities/jwt_token.dart';
import 'package:meta/meta.dart';

class JwtTokenModel extends JwtToken {
  final String token;
  final String refreshToken;
  final int tokenExpire;

  JwtTokenModel({@required this.token, this.refreshToken, this.tokenExpire}) : super(token: token);

  factory JwtTokenModel.fromJson(Map<String, dynamic> json) {
    return JwtTokenModel(
      token: json['token'],
      refreshToken: json['refreshToken'],
      tokenExpire: json['tokenExpire'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'token': token,
      'refreshToken': refreshToken,
      'tokenExpire': tokenExpire,
    };
  }
}
