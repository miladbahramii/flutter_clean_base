import 'dart:convert';
import 'package:flutter_clean_base/core/device/device_info.dart';
import 'package:flutter_clean_base/core/enviorments/env.dart';
import 'package:flutter_clean_base/core/error/exeptions.dart';
import 'package:flutter_clean_base/core/network/request.dart';
import 'package:flutter_clean_base/features/login/data/models/jwt_token_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

abstract class LoginRemoteDataSource {
  Future<JwtTokenModel> getToken(String username, String password);
  Future<void> invalidateToken(String token);
  Future<void> refreshToken(String token);
}

class LoginRemoteDataSourceImpl implements LoginRemoteDataSource {
  final http.Client client;
  final DeviceInfo deviceInfo;
  final FlutterSecureStorage secureStorage;
  final Request request;

  LoginRemoteDataSourceImpl(
      {@required this.client,
      @required this.deviceInfo,
      @required this.secureStorage,
      @required this.request});

  @override
  Future<JwtTokenModel> getToken(String username, String password) async {
    final _deviceInfo = await deviceInfo.info;
    final String infoText = '${_deviceInfo.os}_${_deviceInfo.model}';
    final url = '${Env.url}/IdentityServer/api/v1/IdentityServer/login';

    final http.Response response = await request.post(url, {}, {
      'userName': 'frontuser',
      'password': 'selected@frontuser',
      'subject': 'subject',
      'deviceInfo': infoText
    });

    return JwtTokenModel.fromJson(json.decode(response.body));
  }

  @override
  Future<void> refreshToken(String token) {
    // TODO: implement refreshToken
    throw UnimplementedError();
  }

  @override
  Future<void> invalidateToken(String token) async {
    print('token is $token');
    final String url = '${Env.url}/IdentityServer/api/v1/IdentityServer/logout';
    await request.post(
        url,
        {'Authorization': 'Bearer $token', 'Content-Type': 'application/json'},
        {});
  }
}
