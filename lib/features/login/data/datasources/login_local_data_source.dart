// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_clean_base/core/error/exeptions.dart';
import 'package:flutter_clean_base/core/utils/date_convertor.dart';
import 'package:flutter_clean_base/core/utils/input_convertor.dart';
import 'package:flutter_clean_base/features/login/domain/entities/jwt_token.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';
import '../models/jwt_token_model.dart';

abstract class LoginLocalDataSource {
  Future<void> cacheToken(JwtTokenModel token);
  Future<JwtToken> getToken();
  Future<void> deleteToken();
}

class LoginLocalDataSourceImpl implements LoginLocalDataSource {
  final FlutterSecureStorage secureStorage;
  final DateConvertor dateConvertor;
  final InputConvertor inputConvertor;

  LoginLocalDataSourceImpl(
      {@required this.secureStorage,
      @required this.dateConvertor,
      @required this.inputConvertor});

  @override
  Future<void> cacheToken(JwtTokenModel jwt) async {
    print('token expire is: ${jwt.tokenExpire}');
    final tokenExpire = dateConvertor.increaseNowWithMinutes(jwt.tokenExpire);

    secureStorage.write(key: 'jwt_token', value: jwt.token);
    secureStorage.write(key: 'refresh_token', value: jwt.refreshToken);
    secureStorage.write(key: 'token_expire', value: tokenExpire.toString());
  }

  @override
  Future<JwtToken> getToken() async {
    final tokenExpire = await secureStorage.read(key: 'token_expire');
    return JwtToken(
        token: await secureStorage.read(key: 'jwt_token'),
        refreshToken: await secureStorage.read(key: 'refresh_token'),
        expireDate: tokenExpire);
  }

  @override
  Future<void> deleteToken() async {
    final token = await secureStorage.read(key: 'jwt_token');
    if (token != null)
      await _clearCache();
    else
      throw CacheException();
  }

  Future<void> _clearCache() async {
    secureStorage.delete(key: 'jwt_token');
    secureStorage.delete(key: 'refresh_token');
    secureStorage.delete(key: 'token_expire');
  }
}
