import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_base/app_localizations.dart';
import 'package:flutter_clean_base/features/login/presentation/cubit/login_cubit.dart';

class HomeDisplay extends StatefulWidget {
  @override
  _HomeDisplayState createState() => _HomeDisplayState();
}

class _HomeDisplayState extends State<HomeDisplay> {

  void submitForm() async {
    context.bloc<LoginCubit>().logoutUser();
    Navigator.pushReplacementNamed(context, '/login');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Center(
              child: Text(
            'HOME Page',
            style: TextStyle(fontSize: 22),
          )),
          RaisedButton(
            onPressed: submitForm,
            color: Colors.red,
            child: Text(
              AppLocalizations.of(context).translate('logout'),
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
