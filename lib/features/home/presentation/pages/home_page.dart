import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_base/app_localizations.dart';
import 'package:flutter_clean_base/features/home/presentation/widgets/home_display.dart';
import 'package:flutter_clean_base/features/login/presentation/cubit/login_cubit.dart';
import 'package:flutter_clean_base/injection_container.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => sl<LoginCubit>(),
          child: HomeDisplay(),
        ),
        appBar:
            AppBar(title: Text(AppLocalizations.of(context).translate('home'))),
      ),
    );
  }
}
